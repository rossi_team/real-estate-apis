from rest_framework.serializers import ModelSerializer

from .models import PropertyFeature


class PropertySerializer(ModelSerializer):
    class Meta:
        model = PropertyFeature
        exclude = ['property_id']

