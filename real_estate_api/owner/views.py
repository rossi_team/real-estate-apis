
from rest_framework.generics import GenericAPIView, CreateAPIView

from .models import PropertyFeature
from .serializer import PropertySerializer


class PropertyView(CreateAPIView):
    model = PropertyFeature
    serializer_class = PropertySerializer

