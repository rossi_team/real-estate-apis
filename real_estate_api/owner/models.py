from django.db import models
from django.conf import settings


class PropertyFeature(models.Model):
    CHOICE = (
        ('RT','residential'),
        ('CL', 'commercial')
        
    )
    property_id = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    property_description = models.TextField()
    property_address = models.CharField(max_length=300)
    property_image_1 = models.ImageField(blank=False)
    property_image_2 = models.ImageField(blank=True)
    property_image_3 = models.ImageField(blank=True)
    property_type = models.CharField(max_length=250, choices=CHOICE)
    prop_stats = models.BooleanField(default=False)
    created  = models.DateTimeField(auto_now_add=True)



    def __str__(self):
        return str(self.property_id)


    