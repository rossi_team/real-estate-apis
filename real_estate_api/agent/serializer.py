from rest_framework import serializers

from .models import AgentListing

class AgentSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgentListing
        fields = '__all__'