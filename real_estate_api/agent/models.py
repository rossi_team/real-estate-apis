from django.db import models
from django.conf import settings

# Create your models here.

class AgentListing(models.Model):
    hotel_id = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
