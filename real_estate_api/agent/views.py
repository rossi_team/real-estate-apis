from rest_framework.generics import (CreateAPIView, RetrieveUpdateAPIView,
RetrieveDestroyAPIView, ListAPIView
)
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import SessionAuthentication



from .models import AgentListing
from .serializer import AgentSerializer
from .permission import IsAgent

class CreateHotelPost(CreateAPIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAgent,)
    queryset = AgentListing
    serializer_class = AgentSerializer

    def create(self, request, *args, **kwarg):
        serializer = self.get_serializer(data=request.data, context=self.get_serializer_context()['request'])
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class RetrieveHotelPost(RetrieveUpdateAPIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAgent, )
    queryset = AgentListing
    serializer_class = AgentSerializer
    lookup_field = 'title'



class DeleteHotelPost(RetrieveDestroyAPIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAgent,)
    queryset = AgentListing
    serializer_class = AgentSerializer
    lookup_field = 'title'

class ListHotelPost(ListAPIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAgent,)
    queryset = AgentListing
    serializer_class = AgentSerializer

    def get_queryset(self):
        user = self.request.user
        return self.queryset.objects.filter(hotel_id=user).order_by('-title')
