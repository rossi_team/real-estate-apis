from rest_framework.generics import (CreateAPIView, RetrieveUpdateAPIView,
RetrieveDestroyAPIView, ListAPIView
)
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from .models import HotelListing
from .serializer import CreateHotelPostSerializer
from .permission import IsHotel

class CreateHotelPost(CreateAPIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsHotel)
    queryset = HotelListing
    serializer_class = CreateHotelPostSerializer

    def create(self, request, *args, **kwarg):
        serializer = self.get_serializer(data=request.data, context=self.get_serializer_context()['request'])
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class RetrieveHotelPost(RetrieveUpdateAPIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsHotel)
    queryset = HotelListing
    serializer_class = CreateHotelPostSerializer
    lookup_field = 'pk'



class DeleteHotelPost(RetrieveDestroyAPIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsHotel)
    queryset = HotelListing
    serializer_class = CreateHotelPostSerializer
    lookup_field = 'pk'

class ListHotelPost(ListAPIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsHotel)
    queryset = HotelListing
    serializer_class = CreateHotelPostSerializer

    def get_queryset(self):
        user = self.request.user
        return self.queryset.objects.filter(hotel_id=user).order_by('-title')
