from rest_framework import serializers

from .models import HotelListing
from accounts.models import HotelInfo

def get_hotel_data(user_id):
    queryset = HotelInfo.objects.get(user=user_id)
    return queryset


class CreateHotelPostSerializer(serializers.ModelSerializer):

    class Meta:
        model = HotelListing
        exclude = ['website', 'hotel_name',  'hotel_id']
    
 
    def create(self, validated_data):
        user = None
        request = self.context.get('request', None)
        if request and hasattr(request, 'user'):
            user = request.user
            print(user)
            get_hotel_name = get_hotel_data(user)
            hotel_post = HotelListing.objects.create(hotel_id=user, 
            website=get_hotel_name.hotel_website, hotel_name=get_hotel_name.hotel_name,
            **validated_data)
            return hotel_post
            
        

    
   


