# Generated by Django 2.0 on 2018-06-23 15:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hotel', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='hotellisting',
            name='num',
            field=models.CharField(blank=True, max_length=230),
        ),
    ]
