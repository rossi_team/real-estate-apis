from django.db import models
from django.conf import settings



class HotelListing(models.Model):
    hotel_id = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    hotel_name = models.CharField(max_length=250, default='')
    title = models.CharField(max_length=250)
    detail = models.TextField()
    address = models.CharField(max_length=300)
    website = models.URLField()
    image = models.ImageField(upload_to='media')
    email = models.EmailField()
    pub_date = models.DateTimeField(auto_now_add=True)
    phone_number = models.CharField(max_length=200)
    phone_number2 = models.CharField(max_length=200, blank=True, null=True)
    

    def __str__(self): 
        return self.title

