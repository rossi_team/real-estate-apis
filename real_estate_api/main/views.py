from rest_framework.generics import RetrieveUpdateAPIView, CreateAPIView
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated


from .serializer import UserPortFolioSerializer

from accounts.models import UserPortFolio

class UpdateUserPortFolio(RetrieveUpdateAPIView):
    authentication_classes = (SessionAuthentication,)
    serializer_class = UserPortFolioSerializer
    permission_classes = (IsAuthenticated,)
    queryset = UserPortFolio
    lookup_field = 'pk'
