from django.urls import path

from . import views

urlpatterns = [
    path('update-portfolio/<int:pk>/', views.UpdateUserPortFolio.as_view(), name='update-portfolio')
]