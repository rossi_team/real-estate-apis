from rest_framework import serializers

from accounts.models import UserPortFolio

class UserPortFolioSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserPortFolio
        exclude = ('user',)
