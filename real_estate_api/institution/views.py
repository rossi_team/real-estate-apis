from rest_framework.generics import (CreateAPIView, RetrieveUpdateAPIView,
RetrieveDestroyAPIView, ListAPIView
)
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import TokenAuthentication

from .models import DeveloperListing
from .serializer import DeveloperSerializer
from .permission import IsDeveloper

class CreateHotelPost(CreateAPIView):
    #authentication_classes = (TokenAuthentication,)
    #permission_classes = (HotelUser,)
    queryset = DeveloperListing
    serializer_class = DeveloperSerializer

    def create(self, request, *args, **kwarg):
        serializer = self.get_serializer(data=request.data, context=self.get_serializer_context()['request'])
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class RetrieveHotelPost(RetrieveUpdateAPIView):
    #authentication_classes = (TokenAuthentication,)
    #permission_classes = (HotelUser,)
    queryset = DeveloperListing
    serializer_class = DeveloperSerializer
    lookup_field = 'title'



class DeleteHotelPost(RetrieveDestroyAPIView):
    #authentication_classes = (TokenAuthentication,)
    #permission_classes = (HotelUser,)
    queryset = DeveloperListing
    serializer_class = DeveloperSerializer
    lookup_field = 'title'

class ListHotelPost(ListAPIView):
    #authentication_classes = (TokenAuthentication,)
    #permission_classes = (HotelUser,)
    queryset = DeveloperListing
    serializer_class = DeveloperSerializer

    def get_queryset(self):
        user = self.request.user
        return self.queryset.objects.filter(developer_id=user).order_by('-title')
