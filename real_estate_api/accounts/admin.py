from django.contrib import admin
from django.conf import settings
from django.contrib.auth import get_user_model
from .models import UserPortFolio, HotelInfo, Supplier, Customer, AgentInfo
# Register your models here.

admin.site.register(get_user_model())

admin.site.register(UserPortFolio)
admin.site.register(HotelInfo)
admin.site.register(Supplier)
admin.site.register(Customer)
admin.site.register(AgentInfo)