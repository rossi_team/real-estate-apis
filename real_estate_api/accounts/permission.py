from rest_framework.permissions import BasePermission

class ValidateSession(BaseException):

    def has_permission(self, request, view):
        return 'otp_data' in request.session

class ValidateUser(BaseException):

    def has_permission(self, request, view):
        return 'email' in request.session  or 'user_otp' in request.session or 'otp_code' in request.session

class ValidateResetPass(BaseException):
    def has_permission(self, request, view):
        return 'email_data' in request.session or 'user' in request.session

class ValidateEmailReset(BaseException):
    def has_permission(self, request, view):
        return 'code' in request.session


class ValidateResendOTP(BaseException):
    def has_permission(self, request, view):
        return 'user_otp' in request.session or 'otp_code' in request.session or 'email' in request.session

class ValidateEmailCode(BaseException):
    def has_permission(self, request, view):
        return 'email_code' in request.session

class ValidateEmailResendCode(BaseException):
    def has_permission(self, request, view):
        return 'user' in request.session
    