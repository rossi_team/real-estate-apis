from django.contrib.gis.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.conf import settings

from rest_framework import exceptions

from .managers import UserManager
from .utils import Auth


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=120, unique=True)
    authy_id = models.CharField(max_length=250)
    password = models.CharField(max_length=250)
    is_confirmed = models.BooleanField(default=False)
    phone_number_verified = models.BooleanField(default=False)
    contact_number = models.CharField(max_length=250)
    is_property_owner = models.BooleanField(default=False)
    is_agent = models.BooleanField(default=False)
    is_developer = models.BooleanField(default=False)
    is_institute = models.BooleanField(default=False)
    is_government = models.BooleanField(default=False)
    is_customer = models.BooleanField(default=False)
    is_hotelier = models.BooleanField(default=False)
    is_valuer = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_supplier = models.BooleanField(default=False)
    


    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()


    def email_user(self, email):
        pass


    @staticmethod
    def send_otp_code(authy_id):
        auth = Auth()
        if hasattr(auth, 'send_code'):
            send_sms = auth.send_code(authy_id)
            if send_sms.errors():
                raise exceptions.ValidationError({'detail': 'failed to validate number'})
            else:
                return send_sms

class SignUPInfo(models.Model):
    full_name = models.CharField(max_length=250)
    identity = models.ImageField(upload_to='media')
    contact_address = models.CharField(max_length=300)
    cac_number = models.IntegerField()
    
    class Meta:
        abstract = True


class HotelInfo(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    hotel_name = models.CharField(max_length=25, unique=True)
    cac_number = models.IntegerField(unique=True)
    hotel_website = models.URLField(unique=True)


    def __str__(self):
        return str(self.user)

class PropertyOwnerInfo(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=250)
    contact_address = models.CharField(max_length=250)

    def __str__(self):
        return str(self.user)
    

class AgentInfo(SignUPInfo):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.user)


class DeveloperInfo(SignUPInfo):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.user)

class GovernmentInfo(SignUPInfo):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    identity = models.ImageField(upload_to='media')


class Institute(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    website = models.URLField()
    institute_name = models.CharField(max_length=250)
    contact_address = models.CharField(max_length=250)
    cac_number = models.CharField(max_length=250)
    identity = models.ImageField(upload_to='media')
    website = models.URLField()


    def __str__(self):
        return str(self.user)

class Valuer(SignUPInfo):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.user)

class Customer(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=250)
    contact_address = models.CharField(max_length=150, blank=True)
    notify = models.BooleanField(default=False)
    

    def __str__(self):
        return str(self.user)

class Supplier(SignUPInfo):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.user)

class UserPortFolio(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=250)
    contact_address = models.CharField(max_length=250)
    email = models.EmailField()
    phone_number = models.CharField(max_length=250)
    professional_summary = models.TextField()
    education = models.TextField()
    work_experince = models.TextField()
    skill_set = models.TextField()
    
    def __str__(self):
        return str(self.full_name)




