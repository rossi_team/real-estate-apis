import json
import re

from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.utils.crypto import get_random_string
from django.conf import settings
from django.core import serializers
from django.core.signing import TimestampSigner
from django.core.signing import BadSignature, SignatureExpired

from rest_framework.generics import GenericAPIView, CreateAPIView, RetrieveUpdateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status, exceptions
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authentication import TokenAuthentication


from .permission import (
    ValidateSession, ValidateUser,  
    ValidateResetPass, ValidateResendOTP, 
    ValidateEmailCode, ValidateEmailResendCode
)
from .utils import Auth
from .serializer import (
    PropertyOwnerSerializer, AgentSerializer,
    DeveloperSerializer,
    HotelierSerializer, CustomerSerializer,
    GovernmentSerializer, PasswordResetNumberSerializer,
    LoginSerializer, OtpSerializer, PasswordResetSerializer,
    SupplierSerializer, ValuerSerializer, InstituteSerializer, PasswordResetEmailSerializer,
    PasswordResetCodeSerializer, 
)
from .validators import ValidityError
from .models import Customer

def password_reset_code():
    code = get_random_string(length=7)
    return code

def check_for_email(email):
    if re.match(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", email):
        return True
    return False


def sign_token():
    token = TimestampSigner(salt='extra')
    return token
    


def generate_safe_token(email):
    signer = sign_token().sign(email)
    return signer


class PropertyOwnerSignUpAPIView(CreateAPIView):
    serializer_class = PropertyOwnerSerializer
    queryset = get_user_model()

    def create(self, request, *args, **kwarg):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(): 
            self.perform_create(serializer)
            request.session['email'] = serializer.validated_data['user']['email']
            request.session.set_expiry(300)
            return Response({'message':'True', 'res':True, 'reason': 'signed up successful'}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'false', 'res': False, 'reason': 'invalid data format'}, status=status.HTTP_200_OK)

class AgentSignUpAPIView(CreateAPIView):
    serializer_class = AgentSerializer
    queryset = get_user_model()
  
    def create(self, request, *args, **kwarg):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(): 
            self.perform_create(serializer)
            request.session['email'] = serializer.validated_data['user']['email']
            request.session.set_expiry(300)
            return Response({'message':'True', 'res':True, 'reason': 'signed up successful'}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'false', 'res': False, 'reason': 'invalid data format'}, status=status.HTTP_200_OK)


class DeveloperSignUpAPIView(CreateAPIView):
    serializer_class = DeveloperSerializer
    queryset = get_user_model()

    def create(self, request, *args, **kwarg):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(): 
            self.perform_create(serializer)
            request.session['email'] = serializer.validated_data['user']['email']
            request.session.set_expiry(300)
            return Response({'message':'True', 'res':True, 'reason': 'signed up successful'}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'false', 'res': False, 'reason': 'invalid data format'}, status=status.HTTP_200_OK)

class SupplierSignUpAPIView(CreateAPIView):
    queryset = get_user_model()
    serializer_class = SupplierSerializer

    def create(self, request, *args, **kwarg):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid()
        self.perform_create(serializer)
        request.session['email'] = serializer.validated_data['email']
        request.set_expiry(300)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class ValuerSignUpAPIView(CreateAPIView):
    serializer_class = ValuerSerializer
    queryset = get_user_model()

    def create(self, request, *args, **kwarg):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(): 
            self.perform_create(serializer)
            request.session['email'] = serializer.validated_data['user']['email']
            request.session.set_expiry(300)
            return Response({'message':'True', 'res':True, 'reason': 'signed up successful'}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'false', 'res': False, 'reason': 'invalid data format'}, status=status.HTTP_200_OK)


class InstituteSignUpAPIView(CreateAPIView):
    serializer_class = InstituteSerializer
    queryset = get_user_model()

    def create(self, request, *args, **kwarg):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(): 
            self.perform_create(serializer)
            request.session['email'] = serializer.validated_data['user']['email']
            request.session.set_expiry(300)
            return Response({'message':'True', 'res':True, 'reason': 'signed up successful'}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'false', 'res': False, 'reason': 'invalid data format'}, status=status.HTTP_200_OK)

class GovernmentSignUpAPIView(CreateAPIView):
    serializer_class = GovernmentSerializer
    queryset = get_user_model()

    def create(self, request, *args, **kwarg):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(): 
            self.perform_create(serializer)
            request.session['email'] = serializer.validated_data['user']['email']
            request.session.set_expiry(300)
            return Response({'message':'True', 'res':True, 'reason': 'signed up successful'}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'false', 'res': False, 'reason': 'invalid data format'}, status=status.HTTP_200_OK)


class HotelierSignUpAPIView(CreateAPIView):
    serializer_class = HotelierSerializer
    queryset = get_user_model()

    def create(self, request, *args, **kwarg):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(): 
            self.perform_create(serializer)
            request.session['email'] = serializer.validated_data['user']['email']
            request.session.set_expiry(300)
            return Response({'message':'True', 'res':True, 'reason': 'signed up successful'}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'false', 'res': False, 'reason': 'invalid data format'}, status=status.HTTP_200_OK)


class CustomerSignUpAPIView(CreateAPIView):
    serializer_class = CustomerSerializer
    queryset = get_user_model()

    def create(self, request, *args, **kwarg):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            if self.validate_contact_number(serializer.validated_data['user']['contact_number']):
                self.perform_create(serializer)
                email = serializer.validated_data['user']['email']
                request.session['email'] = email
                request.session.set_expiry(5400)
                token = generate_safe_token(email)
                return Response({'message':'True', 'res':True, 'reason': 'signed up successfully', 'token': token}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'false', 'res': False, 'reason': ' email assoicated with an account already'}, status=status.HTTP_200_OK)

    
    def validate_contact_number(self, number):
        get_number = self.queryset.objects.filter(contact_number=number)
        if get_number.exists():
            raise ValidityError({'message': 'number exist', 'res':False, 'reason':'number assoicated with an account already'})
        else:
            return number

class ValidateNumberAPIView(GenericAPIView):

    serializer_class = OtpSerializer

    auth = Auth()

    permission_classes = (ValidateUser,)

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        email = request.session.get('email', None)
        token = request.session.get('token', None)
        if email is not None:
            request.session['user_otp'] = email
            if serializer.is_valid():
                check_email = self.validate_email(email)
                verify_code = self.auth.verify_code(check_email, serializer.validated_data['otp_code'])
                if verify_code.errors():
                    return Response({'message':'False' , 'res': False,'reason': 'Invalid Code'}, status=status.HTTP_200_OK)
                else:
                    request.session.clear()
                    self.update_user(check_email)
                    return Response({'message':'True' ,'res': True, 'reason': 'code verified successfully'}, status=status.HTTP_200_OK)
            else:
                return Response({'res': False, 'reason': 'invalid payload', 'message': 'invalid payload'}, status=status.HTTP_200_OK)
        elif token:
            request.session['user_otp'] = email
            if serializer.is_valid():
                check_email = self.validate_email(token)
                verify_code = self.auth.verify_code(check_email, serializer.validated_data['otp_code'])
                if verify_code.errors():
                    return Response({'message':'False' , 'res': False,'reason': 'Invalid Code'}, status=status.HTTP_200_OK)
                else:
                    request.session.clear()
                    self.update_user(check_email)
                    return Response({'message':'True' ,'res': True, 'reason': 'code verified successfully'}, status=status.HTTP_200_OK)
            else:
                return Response({'res': False, 'reason': 'invalid payload', 'message': 'invalid payload'}, status=status.HTTP_200_OK)
        else:
            return Response({'res': False, 'reason':'no value', 'message': 'no value'}, status=status.HTTP_200_OK)
    
    def validate_email(self, email):
        try:
            user = get_user_model().objects.get(email__iexact=email)
        except ObjectDoesNotExist:
            raise ValidityError({'message': 'False', 'res':False, 'reason':'failed to verify code' })
        else:
            auth_id = user.authy_id
            return auth_id

    @staticmethod
    def update_user(instance):
        user = get_user_model().objects.get(authy_id__iexact=instance)
        user.phone_number_verified = True
        user.save()
        return user


class ResendOTPTokenView(GenericAPIView):
    auth = Auth()
    def get(self, request):
        token = request.query_params.get('token', None)
        reset_token = request.query_params.get('reset_token', None)
        if token is not None:
            try:
                safe_token = sign_token().unsign(token, max_age=5400)
            except (BadSignature, SignatureExpired):
                raise ValidityError({'res': False, 'message': 'expired/invalid token', 'reason': 'invalid token'})
            else:
    
                user_id = self.get_user_auth_id(safe_token)
                send_code = self.auth.send_code(user_id)
                if send_code.ok():
                    request.session['token'] = safe_token
                    return Response({'message': 'True', 'reason': 'code sent succesfully', 'res': True})
                else:
                    return Response({'message': 'code not sent', 'reason': 'Failed to send code', 'res': False})
        elif reset_token:
            try:
                safe_token = sign_token().unsign(token, max_age=5400)
            except (BadSignature, SignatureExpired):
                raise ValidityError({'res': False, 'message': 'expired/invalid token', 'reason': 'invalid token'})
            else:
                send_code = self.auth.send_code(reset_token)
                if send_code.ok():
                    request.session['token'] = reset_token
                    return Response({'message': 'True', 'reason': 'code sent succesfully', 'res': True})
                else:
                    return Response({'message': 'code not sent', 'reason': 'Failed to send code', 'res': False})


        else:
            return Response({'message': 'no token provided', 'reason': 'token was not found', 'res': False})

    @staticmethod
    def get_user_auth_id(email):
        try:
            user = get_user_model().objects.get(email__iexact=email)
        except ObjectDoesNotExist:
            pass
        else:
        
            return user.authy_id


    

class ResendOTPAPIView(GenericAPIView):
    auth = Auth()
    permission_classes = (ValidateResendOTP,)

    def get(self, request):
        email = request.session.get('user_otp', None)
        auth_id = request.session.get('otp_data', None)
        user = request.session.get('email', None)
        if email is not None:
            get_user_id = self.validate_email(email)
            send_code = self.auth.send_code(get_user_id)
            if send_code.errors():
                return Response({'message': 'False', 'res': False, 'reason':'failed to send code' }, status=status.HTTP_200_OK)

            else:
                return Response({'res': True, 'message': 'True', 'reason': 'code was sent succesfully'}, status=status.HTTP_200_OK)
        elif auth_id:
            send_code = self.auth.send_code(auth_id)
            if send_code.errors():
                return Response({'message': 'False', 'res': False, 'reason':'failed to send code' }, status=status.HTTP_200_OK)

            else:
                return Response({'res': True, 'message': 'True', 'reason': 'code was sent succesfully'}, status=status.HTTP_200_OK)


        else:
            raise ValidityError({'error': 'an error occured'})

    def validate_email(self, email):
        try:
            user = get_user_model().objects.get(email__iexact=email)
        except ObjectDoesNotExist:
            raise ValidityError({'message': 'False', 'res': False, 'reason':'failed to send code'})
        else:
            auth_id = user.authy_id
            return auth_id



class PasswordResetNumberAPIView(GenericAPIView):
    model = get_user_model()
    serializer_class = PasswordResetNumberSerializer

    auth = Auth()

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            data = self.validate_number(serializer.validated_data['number'])
            send_sms = self.auth.send_code(data)
            if send_sms.errors():
                return Response({'message': 'False', 'res': False, 'reason': 'failed  send code'}, status=status.HTTP_200_OK)
            else:
                request.session['otp_data'] = data
                request.session.set_expiry(5400)
                token = generate_safe_token(data)
                return Response({'message': 'True', 'res': True, 'reason': 'code sent successfully', 'token': token}, status=status.HTTP_200_OK)
                
        else:
            return Response({'message': 'invalid payload', 'reason': 'error occured', 'res': False}, status=status.HTTP_400_BAD_REQUEST)
    
    @staticmethod
    def validate_number(data):
        try:
            user = get_user_model().objects.get(contact_number=data)
        except ObjectDoesNotExist:
            raise  ValidityError({'reason': 'This number does not belong to any account', 'res': False, 'message': 'number not found'})
        else:
            return user.authy_id

    
class PasswordRestEmailAPIView(GenericAPIView):
    serializer_class  = PasswordResetEmailSerializer
    queryset = get_user_model()

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            validate_email = self.check(serializer.validated_data['email'])
            code = password_reset_code()
            message_body = 'Real Estate password reset code {}'.format(code)
            mail = send_mail('Password reset code', message_body, settings.EMAIL_HOST_USER, [serializer.validated_data['email']], fail_silently=False)
            if mail:
                request.session['email_code'] = code
                request.session['user'] = validate_email
                return Response({'res': True, 'message': 'True', 'reason': 'Mail sent successfully'}, status=status.HTTP_200_OK)
            else: 
                return Response({'res': False, 'message': 'False', 'reason': 'Failed to send mail'}, status=status.HTTP_200_OK)
        else:
            return Response({'res': False, 'message': 'not valid email', 'reason': 'Invalid Email'},  status=status.HTTP_200_OK)
    
    @staticmethod
    def check(instance):
        user = get_user_model().objects.filter(email__iexact=instance)
        if not user.exists():
            raise ValidityError({'res': False, 'message': 'not found', 'reason': 'Email doesnt belong to any account'})
        else:
            #serializer_user = serializers.serialize('json', user)
            return instance

class ResendEmailCodeAPIView(GenericAPIView):
    permission_classes = (ValidateEmailResendCode,)

    def get(self, request):
        user = request.session.get('user', None)

        if user is not None: 
            code = password_reset_code()
            message_body = 'Real Estate password reset code {}'.format(code)
            mail = send_mail('Password reset code', message_body, settings.EMAIL_HOST_USER, [user], fail_silently=False)
            if mail:
                request.session['email_code'] = code
                request.session['user'] = user
                return Response({'res': True, 'message': 'True', 'reason': 'Mail sent successfully'}, status=status.HTTP_200_OK)
            else: 
                return Response({'res': False, 'message': 'False', 'reason': 'Failed to send mail'}, status=status.HTTP_200_OK)



class ConfirmedEmailCodeAPIView(GenericAPIView):
    serializer_class = PasswordResetCodeSerializer
    permission_classes = (ValidateEmailCode,)

    def post(self, request):
        code = request.session.get('email_code', None)
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            if code == serializer.validated_data['code']:
                del request.session['email_code']
                return Response({'res': True, 'message': 'True', 'reason': 'Validated code successfulluy'}, status=status.HTTP_200_OK)
            else: 
                return Response({'res': False, 'message': 'False', 'reason': 'Validated code Failed'}, status=status.HTTP_200_OK)


class PasswordResetOTPConfirmedAPIView(GenericAPIView):
    serializer_class = OtpSerializer
    permission_classes = (ValidateSession,)
    auth  = Auth()

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            code = serializer.validated_data['otp_code']
            data = request.session.get('otp_data', None)
            verify_otp = self.auth.verify_code(data, code)
            if verify_otp.errors():
                return Response({'reason': 'failed to validate otp code', 'message': 'False', 'res':False})
            else:
                del request.session['otp_data']
                request.session['email_data'] = data
                return Response({'res': True, 'reason': 'code verified successfully'}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'False', 'reason': 'error occured', 'res': False}, status=status.HTTP_200_OK)
            
    
class PasswordResetConfirmAPIView(GenericAPIView):

    permission_classes = (ValidateResetPass,)
    serializer_class = PasswordResetSerializer

    def post(self, request):
        number = request.session.get('email_data', None)
        if number is None:
            email = request.session.get('user', None)
            if email is not None:
                serializer = self.get_serializer(data=request.data)
                if serializer.is_valid():
                    new_password = self.change_password_with_email(email, serializer.validated_data['password'])
                    del request.session['user']
                    return Response({'message': 'True','reason': 'password changed successfully', 'res': True}, status=status.HTTP_200_OK)
                else:
                    return Response({'message': 'Failed', 'res': False,  'reason': 'Invalid password'}, status=status.HTTP_200_OK)
            else:
                 return Response({'message': 'Failed', 'res': False,  'reason': 'No value passed in the session'}, status=status.HTTP_200_OK)
        else:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                new_password = self.change_password_with_auth(email, serializer.validated_data['password'])
                del request.session['email_data']
                return Response({'message': 'True','reason': 'password changed successfully', 'res': True}, status=status.HTTP_200_OK)
            else:
                return Response({'message': 'Failed', 'res': False,  'reason': 'Invalid password'}, status=status.HTTP_200_OK)

    
    def change_password_with_auth(self, instance, password):
        try:
            queryset = get_user_model().objects.get(authy_id__iexact=instance)
        except ObjectDoesNotExist:
            raise ValidityError({'reason': 'unable to change password', 'message': 'False', 'res': False})
        else:
            queryset.password = password
            hash_password = queryset.set_password(password)
            queryset.save()
            return queryset

    def change_password_with_email(self, instance, password):
        try:
            queryset = get_user_model().objects.get(email__iexact=instance)
        except ObjectDoesNotExist:
            raise ValidityError({'reason': 'unable to change password', 'message': 'False', 'res': False})
        else:
            queryset.password = password
            hash_password = queryset.set_password(password)
            queryset.save()
            return queryset

class LoginAPIView(GenericAPIView):
    """
    post:
    Logs in a user if the authentication credentials are correct

    """
    serializer_class = LoginSerializer
    queryset = get_user_model()

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            email = serializer.validated_data['email']
            password = serializer.validated_data['password']
            user = authenticate(email=email, password=password)
            if user is None:
                return Response({'message': 'False', 'reason': 'invalid  credentials', 'res': False}, status=status.HTTP_200_OK)

            elif user  and user.is_confirmed and user.phone_number_verified:
                if user.is_customer:
                    try:
                        notification_status = Customer.objects.get(user=user)
                    except ObjectDoesNotExist:
                        login(request, user=user)
                        return Response({'message': 'True', 'reason': 'user account confirmed', 'res':True, 'notify_stat': False}, status=status.HTTP_200_OK)
                    else:
                        login(request, user=user)
                        return Response({'message': 'True', 'reason': 'user account confirmed', 'res':True, 'notify_stat': notification_status.notify}, status=status.HTTP_200_OK)
                else:
                    return Response({'message': 'True', 'reason': 'user account confirmed', 'res':True}, status=status.HTTP_200_OK)

            elif user and not user.is_confirmed and not user.phone_number_verified:
                return Response({'message': 'not confirmed', 'reason': 'user account not confirmed', 'res':False}, status=status.HTTP_200_OK)
            
            elif user and not user.phone_number_verified:
                return Response({'message':  'phone not confirmed', 'reason': 'phone number not verified', 'res': True}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'False', 'reason': 'invalid Password', 'res':False}, status=status.HTTP_200_OK)


class LogoutAPIView(GenericAPIView):
    """
    get:
    Logs out the current user 



    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        logout(request)
        return Response({'message': 'True', 'reason': 'logged out successfully', 'res':True}, status=status.HTTP_200_OK)

