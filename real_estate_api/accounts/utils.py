import os

from django.conf import settings

from authy.api import AuthyApiClient

class Auth:
    auth = AuthyApiClient(settings.API_KEY)

    def create_user(self, email, phone, code=234):
        create = self.auth.users.create(email=email, phone=phone, country_code=code)
        return create

    def send_code(self, auth_id):
        sms_code = self.auth.users.request_sms(auth_id, {'force': True})
        return sms_code


    def verify_code(self, auth_id, token):
        verify_code = self.auth.tokens.verify(auth_id, token, {'force': True})
        return verify_code