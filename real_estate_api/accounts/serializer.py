import re

from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist

from rest_framework import serializers
from rest_framework.response import Response

from .utils import Auth
from .models import (HotelInfo, PropertyOwnerInfo,
AgentInfo, Supplier, Valuer, Institute, GovernmentInfo,
DeveloperInfo,Customer)



def create_auth(email, phone):
    user = Auth()
    create = user.create_user(email, phone)
    if create.ok():
        return create.id
    elif create.errors():
        raise exceptions.ValidationError({'detail': 'Invalid Phone Number'})

def create_user(instance):
    user = get_user_model().objects.create_user(email=instance['email'], 
    contact_number=instance['contact_number'])
    user.set_password(instance['password'])
    return user

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['email', 'password', 'contact_number']
        extra_kwargs = {
            'password': {
                'write_only': True
            }
        }

    '''
    def validate_password(self, value):
        if not re.search(r"^(?=.\d)(?=.[a-z])(?=.*[A-Z]).{8,}$", value):
            raise serializers.ValidationError('invalid passphrase format')
        return value
    '''

class PropertyOwnerSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)

    class Meta:
        model = PropertyOwnerInfo
        fields = ('full_name', 'contact_address', 'user'
        )
        

    def create(self, validated_data):
        user = validated_data.pop('user')
        create_auth_id = create_auth(user['email'], 
        user['contact_number'])
        if create_auth_id:
            user = create_user(instance=user)
            user.is_property_owner = True
            user.authy_id = create_auth_id
            user.save()
            user.send_otp_code(create_auth_id)
            owner = PropertyOwnerInfo.objects.create(user=user, **validated_data)

            return owner


class AgentSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)
   
    class Meta:
        model = AgentInfo
        fields = ('full_name', 'contact_address',  'cac_number', 'identity',
        'user'
        )
        

    def create(self, validated_data):
        user = validated_data.pop('user')
        create_auth_id = create_auth(user['email'], 
        user['contact_number'])
        if create_auth_id:
            user = create_user(instance=user)
            user.is_agent=True
            user.authy_id = create_auth_id
            user.save()
            user.send_otp_code(create_auth_id)
            agent = AgentInfo.objects.create(user=user, **validated_data)
            return agent


class DeveloperSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)

    class Meta:
        model = DeveloperInfo
        fields = ('full_name', 'contact_address', 'cac_number', 'identity', 
        'user'
        )

    def create(self, validated_data):
        user = validated_data.pop('user')
        create_auth_id = create_auth(user['email'], 
        user['contact_number'])
        if create_auth_id:
            user = create_user(instance=user)
            user.is_developer=True
            user.authy_id = create_auth_id
            user.save()
            user.send_otp_code(create_auth_id)
            developer = DeveloperInfo.objects.create(user=user, **validated_data)
            return developer



class GovernmentSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)
    class Meta:
        model = GovernmentInfo
        fields = ('full_name', 'contact_address', 'cac_number', 'identity', 'user'
        )
        
    def create(self, validated_data):
        user = validated_data.pop('user')
        create_auth_id = create_auth(user['email'], 
        user['contact_number'])
        if create_auth_id:
            user = create_user(instance=user)
            user.is_government=True
            user.authy_id = create_auth_id
            user.save()
            user.send_otp_code(create_auth_id)
            government = GovernmentInfo.objects.create(user=user, **validated_data
            )
            return government



class CustomerSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)
    class Meta:
        model = Customer
        fields = ('full_name', 'contact_address', 'user'
        )


    def create(self, validated_data):
        user = validated_data.pop('user')
        create_auth_id = create_auth(user['email'], 
        user['contact_number'])
        if create_auth_id:
            user = create_user(instance=user)    

            user.is_customer=True
            user.is_confirmed=True
            user.authy_id = create_auth_id
            user.save()
            user.send_otp_code(create_auth_id)
            print(create_auth_id)
            customer = Customer.objects.create(user=user, **validated_data)
            return customer

        

class SupplierSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)

    class Meta:
        model = Supplier
        fields = ('full_name', 'cac_number','identity',
        'user'
        )
     


    def create(self, validated_data):
        user = validated_data.pop('user')
        create_auth_id = create_auth(user['email'], 
        user['contact_number'])
        if create_auth_id:
            user = create_user(instance=user)
            user.is_supplier=True
            user.authy_id = create_auth_id
            user.save()
            user.send_otp_code(create_auth_id)
            supplier = Supplier.objects.create(user=user, **validated_data
            )
            return supplier


class HotelierSerializer(serializers.ModelSerializer):

    user = UserSerializer(required=True)

    class Meta:
        model = HotelInfo
        fields = ('hotel_name',
        'cac_number', 'hotel_website', 'user' )
         

    def create(self, validated_data):
        user = validated_data.pop('user')
        create_auth_id = create_auth(user['email'], 
        user['contact_number'])
        if create_auth_id:
            user =  create_user(instance=user)
            user.is_hotelier=True
            user.authy_id = create_auth_id
            user.save()
            user.send_otp_code(create_auth_id)
            hotel_info = HotelInfo.objects.create(user=user, **validated_data)
            return hotel_info
        
    
class InstituteSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)
   
    class Meta:
        model = Institute
        fields = ('institute_name', 'contact_address', 'cac_number', 'website', 'identity',
        'user'
        )
        

    def create(self, validated_data):
        user = validated_data.pop('user')
        create_auth_id = create_auth(user['email'], 
        user['contact_number'])
        if create_auth_id:
            user = create_user(instance=user)
            user.is_institute=True
            user.authy_id = create_auth_id
            user.save()
            user.send_otp_code(create_auth_id)
            institute =Institute.objects.create(user=user, **validated_data)
            
            return institute


class ValuerSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)

    class Meta:
        model = Valuer
        fields = ('full_name', 'contact_address', 'cac_number',  'identity',
        'user'
        )


    def create(self, validated_data):
        user = validated_data.pop('user')
        create_auth_id = create_auth(user['email'], 
        user['contact_number'])
        if create_auth_id:
            user = create_user(instance=user)
            user.is_valuer=True
            user.authy_id = create_auth_id
            user.save()
            user.send_otp_code(create_auth_id)
            valuer = Valuer.objects.create(user=user, **validated_data)
            return valuer


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(required=True)

    '''
    def validate_password(self, value):
        if not re.search(r"^(?=.\d)(?=.[a-z])(?=.*[A-Z]).{8,}$", value):
            raise serializers.ValidationError('invalid passphrase format')
        return value
    '''
    

class OtpSerializer(serializers.Serializer):
    otp_code = serializers.CharField(required=True)


class PasswordResetNumberSerializer(serializers.Serializer):
    number = serializers.CharField(required=True)


class PasswordResetSerializer(serializers.Serializer):
    password = serializers.CharField(required=True, write_only=True)

    '''
    def validate_password(self, value):
        if not re.search(r"^(?=.\d)(?=.[a-z])(?=.*[A-Z]).{8,}$", value):
            raise serializers.ValidationError('invalid passphrase format')
        return value
    '''


class PasswordResetEmailSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)


class PasswordResetCodeSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=200)

