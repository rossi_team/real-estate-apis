from django.db import models
from django.conf import settings

# Create your models here.
class GovermentListing(models.Model):
    goverment_id = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=250)
    detail = models.TextField()
    pub_date = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='images', null=True)


    def __str__(self):
        return self.title


