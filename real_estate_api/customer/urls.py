from django.urls import path

from . import views

urlpatterns = [
    path('update-notification/', views.NotifcationAPIView.as_view(), name='notify'),
    path('properties/', views.ListPropertyAPIView.as_view(), name='property'),
    path('search-hotel/', views.SearchHotelAPIView.as_view(), name='search'),
    path('agents/', views.ListPropertyAgentAPIView.as_view(), name='agents'),
    path('property/<str:id>/', views.ViewPropertyAPIView.as_view(), name='view')
]