from rest_framework.generics import (
    GenericAPIView, get_object_or_404, ListAPIView, RetrieveAPIView
    )

from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.filters import SearchFilter


from accounts.models import Customer,  HotelInfo, AgentInfo
from accounts.validators import ValidityError
from owner.models import PropertyFeature
from owner.serializer import PropertySerializer

from .permission import IsCustomer
from .serializer import HotelSerializer, AgentSerializer



class NotifcationAPIView(GenericAPIView):
    authentication_classes = (SessionAuthentication, )
    permission_classes = (IsAuthenticated, IsCustomer)

    def get(self, request):
        user = get_object_or_404(Customer, user=request.user)
        user.notify = True
        user.save()
        return Response({'res': True, 'message': 'True', 'reason': 'update okay'}, status=status.HTTP_200_OK)


class ListPropertyAPIView(ListAPIView):
    queryset = PropertyFeature
    serializer_class = PropertySerializer
    permission_classes = (IsAuthenticated, IsCustomer)
    authentication_classes = (SessionAuthentication,)

    def get_queryset(self):
        prop_type = self.request.query_params.get('search_type', None)
        if prop_type is not None and  prop_type == 'CT' or prop_type == 'RT':
            queryset = self.queryset.objects.filter(property_type__iexact=prop_type).exclude(prop_stats=True).all().order_by('-created')
            return queryset
        else:
            raise ValidityError({'res': False, 'message': 'False', 'reason': 'No params found/invalid param'})



class SearchHotelAPIView(ListAPIView):
    permission_classes = (IsAuthenticated, IsCustomer)
    authentication_classes = (SessionAuthentication,)
    serializer_class = HotelSerializer
    queryset = HotelInfo.objects.all()
    search_fields = ('hotel_name',)
    filter_backends =  (SearchFilter,)
    


class ViewPropertyAPIView(RetrieveAPIView):
    permission_classes = (IsAuthenticated, IsCustomer)
    authentication_classes = (SessionAuthentication,)
    serializer_class = PropertySerializer
    queryset = PropertyFeature
    lookup_url_kwarg = 'id'


class ListPropertyAgentAPIView(ListAPIView):
    permission_classes = (IsAuthenticated, IsCustomer)
    authentication_classes = (SessionAuthentication,)
    queryset = AgentInfo.objects.all()
    serializer_class = AgentSerializer

