from rest_framework.serializers import ModelSerializer

from accounts.models import HotelInfo, AgentInfo
from accounts.serializer import UserSerializer

class HotelSerializer(ModelSerializer):
    class Meta:
        model = HotelInfo
        excl = ('id', 'user', 'cac_number')

    
class AgentSerializer(ModelSerializer):
    user = UserSerializer(required=True)
    class Meta:
        model = AgentInfo
        fields = ('user', 'full_name', 'contact_address')